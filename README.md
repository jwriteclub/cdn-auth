# [CDN Auth](https://bitbucket.org/jwriteclub/cdn-auth/)

[![Build Status](https://api.shippable.com/projects/546f17d2d46935d5fbbe385e/badge?branchName=master)](https://app.shippable.com/projects/546f17d2d46935d5fbbe385e/builds/latest)

This library provides methods for generating authorization tokens for CDN links on a number of CDNs. Contributions are gratefully accepted for additional CDNs which are not natively supported by this library.

Developed and maintained by [SendFaster](https://sendfaster.com/content-delivery).

### Supported CDNs

1. Level 3
1. Highwinds
1. SendFaster*
1. MaxCDN*
1. All NginX based CDNs

*: Via the NginX module.

### Usage

In general somewhere early in your initialization code, you should call `CDN::setProvider(new Provider)` on each of the CDN types you will be using, passing in an appropriate provider for the specified CDN type
For example, `\CDNFaster\Level3CDN::setProvider(new \CDNFaster\StaticLevel3CDNAuthProvider(time()+60, "super_secret"));` to setup the Level 3 CDN provider.

Once you've initialized all of the providers you plan to use, signing URLs is is easy as typing `CDN::signUrl("URL")`. For example, `\CDNFaster\SendFasterCDN::signUrl("http://domain.com/file.mp4")`, using the
SendFaster CDN as an example.

#### Unknown CDN Types using NginX

In general, you should be able to use the `NginxBasedCDN` implementation with any CDN using NginX on the edge. You should explore the `setSignatureParamName()` and `setExpirationParamName` in order to customize
the URL generated to the particular settings of your CDN.

#### Storing CDN Credentials in a Database/File/Etcd

Provided as part of this project are very simple "Static" auth providers which take string literals and provide them to the CDN implementations. In the event that you need to provide CDN authorization parameters
from a configuration system or any other external source, simply implement `ICDNAuthProvider` (or the appropriate extended version, for instance `ILevel3CDNAuthProvider` for the Level3CDN). These interfaces
provide a few lightweight getters, such as `getExpiration()` and `getSecret()`. Simply implement these functions to wrap to your existing configuration management scheme.


### Requirements

PHP: 5.3+

This project does not require any external dependencies for normal use. [PHPUnit](https://phpunit.de/) 4+ is required to run the unit tests. Automated tests are run on PHP 5.3, 5.4, 5.5 and 5.6. This project *may*
work on older versions of PHP, but compatibility is not tested.

### Installation

#### Composer

- Install [composer](http://getcomposer.org/download/).
- Create a `composer.json` file in your project, which contains the following snippet:
```json
{
    ...
    "require": {
        "jwriteclub/cdn-auth": "dev-master"
    }
    ...
}
```
- In the same folder as your `composer.json` file run `php composer.phar install` or `composer install`.

#### Manual

- Check out the source code into a folder of your choice.
- Add a line to your includes with the following snippet:
```php
require_once("PATH_TO_PROJECT/examples/autoload.php");
```

### Testing

Tests are powered by PHPUnit. There is a `phpunit.xml` file correctly configured in the project root, so simply running `phpunit` from the project root will execute the tests.

### Contributions

Contributions of additional CDNs or bug fixes are welcomed. Please make sure that all contributions contain relevant tests and open a pull request. By contributing you accept the MIT license for your code and agree to contribute your code irrevocably to SendFaster, Inc. Furthermore, you agree to hold SendFaster, Inc and it's employees harmless from any patent or copyright infringement contained in the code you contribute.

### License

The CDN Auth library is licensed under the [MIT License](./LICENSE.md).