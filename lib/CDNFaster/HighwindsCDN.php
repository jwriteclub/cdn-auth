<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the terms of the LICENSE file included
 * in it's distribution.
 */

namespace CDNFaster;

class HighwindsCDN implements ICDN {
    /**
     * Signs a bare CDN URL and makes it a signed URL ready for display to the client.
     * @param string $url The URL to sign.
     * @param ICDNAuthProvider $provider A repository of secret information to generate the signature.
     * @throws \Exception In the event that the provider is not of a valid provider type
     * @return string The signed CDN url.
     */
    public static function signUrl($url, ICDNAuthProvider $provider = null) {
        if ($provider == null) {
            $provider = self::$sProvider;
        }

        if ($provider == null) {
            throw new \Exception("No auth provider set or passed");
        }

        if (!$provider instanceof IHighwindsAuthProvider) {
            throw new \Exception("Invalid CDN Auth provider type");
        }

        $parts = parse_url($url);

        $link = "";
        if (isset($parts['path']) && $parts['path'] !== null) {
            $link = $parts['path'];
        }

        $str_to_sign = $link;

        if (isset($parts['query']) && $parts['query'] !== null) {
            $parts['query'] .= "&";
        } else {
            $parts['query'] = "";
        }
        $parts['query'] .= $provider->getExpirationParamName() . "=" .$provider->getExpiration();
        $str_to_sign .= '?'.$parts['query'];
        $link .= "?".$parts['query'];

        $str_to_sign .= '&' . $provider->getSignatureParamName() . '='. $provider->getSharedSecret();
        $signature = md5($str_to_sign);
        $link .= '&' . $provider->getTokenParamName() . '=' . $signature;

        $ret = $parts['scheme']."://".$parts['host'];
        if (isset($parts['port']) && $parts['port'] !== null) {
            $ret .= ':'.$parts['port'];
        }
        $ret .= $link;

        return $ret;
    }

    /** @var ICDNAuthProvider */
    protected static $sProvider;
    /**
     * Sets a default provider to be used for future requests to {@link #signUrl}.
     * @param ICDNAuthProvider $provider The default provider to use.
     */
    public static function setProvider(ICDNAuthProvider $provider) {
        self::$sProvider = $provider;
    }
}