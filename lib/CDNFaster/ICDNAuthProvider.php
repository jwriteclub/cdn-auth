<?php
/*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* This software is licensed under the terms of the LICENSE file included
* in it's distribution.
*/

namespace CDNFaster;

interface ICDNAuthProvider {
    /**
     * Provides the hash function which the ICDN will use
     * to compute a complete hash.
     * @return string The hash function to use
     */
    public function getHashFunction();

    /**
     * Determines how long the signed URL will be valid for.
     * @return int The unixtime at which the URL should expire.
     */
    public function getExpiration();

    /**
     * @param $expirationParamName
     * @return void
     */
    public function setExpirationParamName($expirationParamName);

    /**
     * @return string
     */
    public function getExpirationParamName();

    /**
     * @param $signatureParamName
     * @return void
     */
    public function setSignatureParamName($signatureParamName);

    /**
     * @return string
     */
    public function getSignatureParamName();
} 