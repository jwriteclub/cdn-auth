<?php
/*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* This software is licensed under the terms of the LICENSE file included
* in it's distribution.
*/

namespace CDNFaster;

/**
 * Class StaticLevel3CDNAuthProvider implements a completely static, in memory
 * provider for authorizing links on the Level 3 CDN.
 * @package CDNFaster
 */
class StaticLevel3CDNAuthProvider implements ILevel3CDNAuthProvider {
    protected $mExpires;
    protected $mSecret;
    protected $mGeneration;
    protected $_expirationParamName  = 'etime';
    protected $_signatureParamName = 'encoded';

    /**
     * Creates a new static credential store with the specified credentials
     * @param int $expires
     * @param string $secret
     * @param int $generation
     */
    public function __construct($expires, $secret, $generation = 0) {
        $this->mSecret = $secret;
        $this->mGeneration = $generation;
        $this->mExpires = $expires;
    }
    /**
     * Provides the hash function which the ICDN will use
     * to compute a complete hash.
     * @return string The hash function to use
     */
    public function getHashFunction() {
        return "sha1";
    }
    /**
     * Determines how long the signed URL will be valid for.
     * @return int The unixtime at which the URL should expire.
     */
    public function getExpiration() {
        return $this->mExpires;
    }
    /**
     * Gets the secret key for Level3 signing
     * @return string The Level3 secret key
     */
    public function getSecret() {
        return $this->mSecret;
    }
    /**
     * Gets the generation parameter for Level3
     * signing.
     * @return int The generation.
     */
    public function getGeneration() {
        return $this->mGeneration;
    }

    /**
     * @return string
     */
    public function getExpirationParamName() {
       return $this->_expirationParamName;
    }

    /**
     * @return string
     */
    public function getSignatureParamName() {
        return $this->_signatureParamName;
    }

    /**
     * @param $expirationParamName
     * @return void
     */
    public function setExpirationParamName($expirationParamName) {
        $this->_expirationParamName = $expirationParamName;
    }

    /**
     * @param $signatureParamName
     * @return void
     */
    public function setSignatureParamName($signatureParamName) {
        $this->_signatureParamName = $signatureParamName;
    }
}