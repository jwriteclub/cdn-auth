<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the terms of the LICENSE file included
 * in it's distribution.
 */

class HighwindsCDNTest extends \CDNFaster\CDNFasterTestCase {

    /** @var \CDNFaster\StaticHighwindsCDNAuthProvider */
    protected $auth;
    /** @var int */
    protected $expires;
    /** @var string */
    protected $secret;

    protected function setUp() {
        $this->expires = '1234567890';
        $this->secret = 'helloworld';
        $this->auth = new \CDNFaster\StaticHighwindsCDNAuthProvider($this->expires, $this->secret);
    }

    public function testRespectsScheme() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertEquals("custom", $parts['scheme'], "Maintains scheme");
    }

    public function testMaintainsHost() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertEquals("domain.com", $parts['host'], "Maintains host");
    }

    public function testMaintainsPort() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com:1337/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertEquals("1337", $parts['port'], "Maintains port");
    }

    public function testMaintainsPath() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertEquals("/file.ext", $parts['path'], "Maintains path");

        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertFalse(isset($parts['path']), "Maintains no path");
    }

    public function testMaintainsQuery() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext?key=value", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        parse_str($parts['query'], $query);

        $this->assertEquals(3, count($query), "Correct number of query string elements");
        $this->assertTrue(isset($query['key']), "Contains the original query part");
        $this->assertEquals("value", $query['key'], "Preserves the value of original query string items");
    }

    public function testMaintainsQueryOrder() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext?a=first&b=second&c=third", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        parse_str($parts['query'], $query);

        $this->assertEquals(5, count($query), "Correct number of query string elements");

        $items = array("a" => "first", "b" => "second", "c" => "third");

        $i = 0;
        foreach($items as $k => $v) {
            $this->assertEquals($v, array_shift($query));
        }
    }

    /**
     * @expectedException Exception
     */
    public function testEnforcesCorrectAuthProvider() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext?key=value", new \CDNFaster\StaticLevel3CDNAuthProvider($this->expires, $this->secret, 1));
    }

    /**
     * @expectedException Exception
     */
    public function testFailsWhenNoDefaultProviderSet() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext?key=value");
    }

    public function testCreatesCorrrectToken() {
        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        parse_str($parts['query'], $query);

        $this->assertEquals(2, count($query), "Correct number of query string elements");

        $this->assertTrue(isset($query[$this->auth->getExpirationParamName()]), "Contains an expiration");
        $this->assertEquals($this->expires, $query[$this->auth->getExpirationParamName()], "Contains the correct expiration");

        $this->assertTrue(isset($query[$this->auth->getTokenParamName()]), "Contains a token");
        $this->assertEquals("49ca7493f5a56eebc2c0af06a373849d", $query[$this->auth->getTokenParamName()], "Contains the correct signature");
    }

    public function testAllowsDefaultProvider() {
        \CDNFaster\HighwindsCDN::setProvider($this->auth);

        $url = \CDNFaster\HighwindsCDN::signUrl("custom://domain.com/file.ext");

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        parse_str($parts['query'], $query);

        $this->assertEquals(2, count($query), "Correct number of query string elements");
        $this->assertTrue(isset($query[$this->auth->getExpirationParamName()]), "Contains an expiration");
        $this->assertEquals($this->expires, $query[$this->auth->getExpirationParamName()], "Contains the correct expiration");
        $this->assertTrue(isset($query[$this->auth->getTokenParamName()]), "Contains a token");
        $this->assertFalse(isset($query[$this->auth->getSignatureParamName()]), "Doesn't contain the secret");
    }
}