<?php
/*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* This software is licensed under the terms of the LICENSE file included
* in it's distribution.
*/

class Level3CDNTest extends \CDNFaster\CDNFasterTestCase {
    /** @var \CDNFaster\StaticLevel3CDNAuthProvider */
    protected $auth;
    /** @var int */
    protected $expires;
    /** @var string */
    protected $secret;
    /** @var int */
    protected $generation;

    protected function setUp() {
        $this->expires = '1234567890';
        $this->secret = 'helloworld';
        $this->generation = 2;
        $this->auth = new \CDNFaster\StaticLevel3CDNAuthProvider($this->expires, $this->secret, $this->generation);
    }

    public function testRespectsScheme() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertEquals("custom", $parts['scheme'], "Maintains scheme");
    }

    public function testMaintainsHost() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertEquals("domain.com", $parts['host'], "Maintains host");
    }

    public function testMaintainsPort() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com:1337/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertEquals("1337", $parts['port'], "Maintains port");
    }

    public function testMaintainsPath() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertEquals("/file.ext", $parts['path'], "Maintains path");

        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        $this->assertFalse(isset($parts['path']), "Maintains no path");
    }

    public function testMaintainsQuery() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext?key=value", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        parse_str($parts['query'], $query);

        $this->assertEquals(3, count($query), "Correct number of query string elements");
        $this->assertTrue(isset($query['key']), "Contains the original query part");
        $this->assertEquals("value", $query['key'], "Preserves the value of original query string items");
    }

    public function testMaintainsQueryOrder() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext?a=first&b=second&c=third", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        parse_str($parts['query'], $query);

        $this->assertEquals(5, count($query), "Correct number of query string elements");

        $items = array("a" => "first", "b" => "second", "c" => "third");

        $i = 0;
        foreach($items as $k => $v) {
            $this->assertEquals($v, array_shift($query));
        }
    }


    /**
     * @expectedException Exception
     */
    public function testEnforcesCorrectAuthProvider() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext?key=value", new \CDNFaster\StaticNginxBasedCDNAuthProvider(123, "hello"));
    }

    /**
     * @expectedException Exception
     */
    public function testFailsWhenNoDefaultProviderSet() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext?key=value");
    }

    public function testCreatesCorrrectToken() {
        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext", $this->auth);

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        parse_str($parts['query'], $query);

        $this->assertEquals(2, count($query), "Correct number of query string elements");

        $this->assertTrue(isset($query[$this->auth->getExpirationParamName()]), "Contains an expiration");
        $this->assertEquals("20090213233130", $query[$this->auth->getExpirationParamName()], "Contains the correct expiration");

        $this->assertTrue(isset($query[$this->auth->getSignatureParamName()]), "Contains a signature");
        $this->assertEquals("25dcbcf03ef8f95b93e78", $query[$this->auth->getSignatureParamName()], "Contains the correct signature");
        $this->assertEquals($this->auth->getGeneration(), $query[$this->auth->getSignatureParamName()][0], "Correct generation");
    }

    public function testAllowsDefaultProvider() {
        \CDNFaster\Level3CDN::setProvider(new \CDNFaster\StaticLevel3CDNAuthProvider($this->expires, $this->secret));

        $url = \CDNFaster\Level3CDN::signUrl("custom://domain.com/file.ext");

        $parts = parse_url($url);
        $this->assertNotFalse($parts, "Parsed the return value as a URL");

        parse_str($parts['query'], $query);

        $this->assertEquals(2, count($query), "Correct number of query string elements");
        $this->assertTrue(isset($query[$this->auth->getExpirationParamName()]), "Contains an expiration");
        $this->assertEquals('20090213233130', $query[$this->auth->getExpirationParamName()], "Contains the correct expiration");
        $this->assertEquals(0, $query[$this->auth->getSignatureParamName()][0], "Correct default generation");
    }
}