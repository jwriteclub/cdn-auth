<?php
/*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* This software is licensed under the terms of the LICENSE file included
* in it's distribution.
*/

class StaticLevel3CDNAuthProviderTest extends \CDNFaster\CDNFasterTestCase {
    public function testStaticAuthProvider() {
        $expires = time() + 123;
        $provider = new \CDNFaster\StaticLevel3CDNAuthProvider($expires, "helloworld", 1);

        $this->assertEquals("sha1", $provider->getHashFunction(), "Forces SHA-1 hash function");
        $this->assertEquals($expires, $provider->getExpiration(), "Correct expiration time");
        $this->assertEquals("helloworld", $provider->getSecret(), "Correct secret");
        $this->assertEquals(1, $provider->getGeneration(), "Correct generation");
    }

    public function testSignatureParams() {
        $expires = time() + 123;
        $provider = new \CDNFaster\StaticLevel3CDNAuthProvider($expires, "helloworld", 1);

        $provider->setExpirationParamName("expires_at123");
        $provider->setSignatureParamName("signature123");

        $this->assertEquals("expires_at123", $provider->getExpirationParamName(), "Expiration param name overridden");
        $this->assertEquals("signature123", $provider->getSignatureParamName(), "Signature param name overridden");
    }
}