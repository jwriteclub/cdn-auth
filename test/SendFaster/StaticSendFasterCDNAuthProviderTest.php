<?php
/*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* This software is licensed under the terms of the LICENSE file included
* in it's distribution.
*/

class StaticSendFasterCDNAuthProviderTest extends \CDNFaster\CDNFasterTestCase {

    public function testStaticAuthProvider() {
        $expires = time() + 123;
        $provider = new \CDNFaster\StaticSendFasterCDNAuthProvider($expires, "helloworld");

        $this->assertEquals("md5", $provider->getHashFunction(), "Forces MD5 hash function");
        $this->assertEquals($expires, $provider->getExpiration(), "Correct expiration time");
        $this->assertEquals("helloworld", $provider->getSecret(), "Correct secret");
    }

    public function testSignatureParams() {
        $expires = time() + 123;
        $provider = new \CDNFaster\StaticSendFasterCDNAuthProvider($expires, "helloworld");

        $provider->setExpirationParamName("expires_at");
        $provider->setSignatureParamName("signature");

        $this->assertEquals("expires_at", $provider->getExpirationParamName(), "Expiration param name overridden");
        $this->assertEquals("signature", $provider->getSignatureParamName(), "Signature param name overridden");
    }
}